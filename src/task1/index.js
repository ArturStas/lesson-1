function isTimeValid(hours, minutes) {
    if (hours >= 0 && hours <= 24 && minutes >= 0 && minutes <= 60) {
        return true;
    } else {
        return false;
    }
}
console.log(isTimeValid(14, 43));
console.log(isTimeValid(32, 43));
console.log(isTimeValid(15, 57));
console.log(isTimeValid(21, 63));